
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;

public class Wunderground {

        /**
         * @param args
         */
        public static void main(String[] args) throws Exception {
        		// TODO Auto-generated method stub
                // Get from http://www.wunderground.com/weather/api/
                      String key;
                      /*
                      if(args.length < 1) 
                      {
						System.out.println("Enter key: ");
						Scanner in = new Scanner(System.in);
						key = in.nextLine();
                      } 
                      else 
                      {
                      key = "b86fd416f0292a5e";
                      }
                      */
                      
                      //Website that prints the city, state , and zip code.
                      key = "b86fd416f0292a5e";
                      String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";

                      // Connect to the URL
                      URL url = new URL(sURL);
                      HttpURLConnection request = (HttpURLConnection) url.openConnection();
                      request.connect();

                      // Convert to a JSON object to print data
                      JsonParser jp = new JsonParser();
                      JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                      JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

                      // Get some data elements and print them
                      
                        String City = rootobj.get("location").getAsJsonObject().get("city").getAsString();
                                System.out.println(City);
                                
                        String State = rootobj.get("location").getAsJsonObject().get("state").getAsString();
                                System.out.println(State);
                                
                        String Zip = rootobj.get("location").getAsJsonObject().get("zip").getAsString();
                                System.out.println(Zip);

                        
                                
                        String newURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + State + "/" + City + ".json";

                                
                        // Connect to the URL
                        URL NewURL = new URL(newURL);
                        HttpURLConnection request2 = (HttpURLConnection) NewURL.openConnection();
                        request2.connect();                                
                                
                        JsonParser jp2 = new JsonParser();
                        JsonElement root2 = jp2.parse(new InputStreamReader((InputStream) request2.getContent()));
                        JsonObject rootobj2 = root2.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive                      
                        JsonArray todayforecast = rootobj2.get("hourly_forecast").getAsJsonArray(); //.get("hour").getAsJsonArray().get(0).getAsJsonObject().get("fcttext").getAsString();
                        
                        for(int i = 0; i < todayforecast.size(); i++)
                        {
                        	String date = todayforecast.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
                            System.out.println(date);
                            
                        	String cond = todayforecast.get(i).getAsJsonObject().get("condition").getAsString();
                            System.out.println("Current conditions: " + cond);
                            
                        	String temp = todayforecast.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
                            System.out.println("Current temperature: " + temp);
                            
                        	String humid = todayforecast.get(i).getAsJsonObject().get("humidity").getAsString();
                            System.out.println("Current humidity: " + humid + "\n");                            
                        }
                        
                        
        }

}

