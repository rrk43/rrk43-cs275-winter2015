import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.UserAccountManagement.SearchUsers;
import com.temboo.Library.CloudMine.UserAccountManagement.SearchUsers.SearchUsersResultSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Instagram.SearchUsers.SearchUsersInputSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
/*
import com.cloudmine.api.CMApiCredentials;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.rest.callbacks.CMObjectResponseCallback;
import com.cloudmine.api.rest.callbacks.ObjectModificationResponseCallback;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.cloudmine.api.rest.response.CMObjectResponse;
import com.cloudmine.api.rest.response.ObjectModificationResponse;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import com.cloudmine.api.db.LocallySavableCMObject;
*/

public class Calendar extends LocallySavableCMObject {
    private String CalendarID;
    private String OAuth;

    
    Calendar(){}
    public Calendar(String CalendarID, String OAuth) {
        this.CalendarID = CalendarID;
        this.OAuth = OAuth;
    }

    //Your getter and setters determine what gets serialized and what doesn't
    public String getCalendarID() {return CalendarID;}
    public void setMake(String CalendarID) {this.CalendarID = CalendarID;}
    public String getOAuth() {return OAuth;}
    public void setOAuth(String OAuth) {this.OAuth = OAuth;}

public static String executePost(String targetURL, String urlParameters)
	{
		URL url;
		HttpURLConnection connection = null;  
		try {
		      //Create connection
		      url = new URL(targetURL);
		      connection = (HttpURLConnection)url.openConnection();
		      connection.setRequestMethod("POST");
		      connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		      connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		      connection.setRequestProperty("Content-Language", "en-US");  	
		      connection.setUseCaches (false);
		      connection.setDoInput(true);
		      connection.setDoOutput(true);

		      //Send request
		      	DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());
		      	wr.writeBytes (urlParameters);
		      	wr.flush ();
		      	wr.close ();

		      	//Get Response
		      	InputStream is = connection.getInputStream();
		      	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		      	String line;
		      	StringBuffer response = new StringBuffer(); 
		      	while((line = rd.readLine()) != null) 
		      	{
		    	  	response.append(line);
		        	response.append('\r');
		      	}
		      rd.close();
		      return response.toString();
		      //Catch used to terminate connection if it isn't null.
		    } 
		catch (Exception e) 
			{

		      e.printStackTrace();
		      return null;

		    } 
		finally 
			{

		      if(connection != null) 
		      {
		    	  connection.disconnect(); 
		      }
			}
		  }
		
		/**
		 * @param args
		 * @throws TembooException 
		 */
		/*Use your account on Temboo, your application name, and appkey value from Temboo.com. Then use the client_id, client_secret, and redirect_uri from Google Console Developer for Temboo.*/
		public static void main(String[] args) throws TembooException {
			String acctName, appKeyName, appKeyValue;
			String client_id, redirect_uri, code, client_secret, access_token;
			
			Scanner in = new Scanner(System.in);
			
			
			acctName = "rrk43";
			appKeyName = "myFirstApp"; 
			appKeyValue = "da3db495cbf748f7b776b5f7ae337ead"; 		
			client_id = "596658220336-v9886u6tdo8b5ioqans3gsn035sdigud.apps.googleusercontent.com"; 
			client_secret = "OoX6LQvwlKjQtr6gF4UNZAPh"; 				
			redirect_uri = "https://www.temboo.com/oauth_helpers/confirm_google/"; 
		
			// https://developers.google.com/accounts/docs/OAuth2WebServer#offline
			String oauthURL = "https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=" + client_id + "&scope=https://www.googleapis.com/auth/calendar&response_type=code&redirect_uri=" + redirect_uri + "&state=/profile&approval_prompt=force";
			System.out.println("Go to the following URL and obtain the code that you find there.\n" + oauthURL);
			code = in.nextLine();
			
			// Google requires a POST for the next step
			String authorizeURL = "https://accounts.google.com/o/oauth2/token";
			String authorizeParams = "code=" + code + "&client_id=" + client_id + "&client_secret=" + client_secret + "&redirect_uri=" + redirect_uri + "&grant_type=authorization_code";
			String authorizeResponse = executePost(authorizeURL, authorizeParams); 
			
			// Since the response is json, convert to a JSON object to obtain oauth token
			// We could parse it ourselves, but why would we do that? 
			JsonParser oauth_jp = new JsonParser();
			JsonElement oauth_root = oauth_jp.parse(authorizeResponse);
			JsonObject oauth_rootobj = oauth_root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
			access_token = oauth_rootobj.get("access_token").getAsString();
			System.out.println("Got oauth access token: " + access_token);
			
			// It's temboo time
			TembooSession session = new TembooSession(acctName, appKeyName, appKeyValue);
			GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

			// Get an InputSet object for the choreo
			GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

			// Set inputs
			getAllCalendarsInputs.set_ClientSecret(client_secret);
			getAllCalendarsInputs.set_AccessToken(access_token);
			getAllCalendarsInputs.set_ClientID(client_id);

			// Execute Choreo
			GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
			
			//System.out.println(getAllCalendarsResults.get_Response());
			System.out.println("LINE 136");
			// Now parse the json
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(getAllCalendarsResults.get_Response());
			JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

			JsonArray items = rootobj.get("items").getAsJsonArray();
			for(int i = 0; i < items.size(); i++) 
			{
				JsonObject item = items.get(i).getAsJsonObject();
				String id = item.get("id").getAsString();
   		
			}
   		
   		

    		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);
    		// Get an InputSet object for the choreo
    		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();
    		// Set inputs
    		//Client secret, refresh token, and client id from Temboo. Calendar id represents gmail account.
    		getAllEventsInputs.set_ClientSecret("OoX6LQvwlKjQtr6gF4UNZAPh");
    		getAllEventsInputs.set_CalendarID("rkoshy275@gmail.com");
    		getAllEventsInputs.set_RefreshToken("1/QWimpl8XC57_7J-Go_vX3cv2bneNyJ3SbylsGwp0-RZ90RDknAdJa_sgfheVM0XT");
    		getAllEventsInputs.set_ClientID("596658220336-v9886u6tdo8b5ioqans3gsn035sdigud.apps.googleusercontent.com");
    		// Execute Choreo
    		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
    		
   JsonParser jp2 = new JsonParser();
   JsonElement root2 = jp2.parse(getAllEventsResults.get_Response());
   JsonObject rootobj2 = root2.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
   JsonArray items2 = rootobj2.get("items").getAsJsonArray();
   for(int i = 0; i != items2.size(); i++) 
   {
   		String item1 = items2.get(i).getAsJsonObject().get("summary").getAsString();
   		JsonObject item2 = items2.get(i).getAsJsonObject().get("start").getAsJsonObject(); 
   		System.out.println("Title: " + item1 + "\n" + "Time and Date: " + item2.toString() + "\n");
   }

//Saves variables in a query
Calendar Example=new Calendar(CalendarID, OAuth);

LocallySavableCMObject.saveObjects(MyActivity.this, Arrays.asList(CalendarID OAuth), new Response.Listener<ObjectModificationResponse>() {
    @Override
    public void onResponse(ObjectModificationResponse objectModificationResponse) {

    }
});

//Gets the files
LocallySavableCMObject.saveObjects(MyActivity.this, Arrays.asList(honda, toyota), new Response.Listener<ObjectModificationResponse>() {
    @Override
    public void onResponse(ObjectModificationResponse objectModificationResponse) {

    }
});



}
}