import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class TembooTweets {

	public static String Post(String targetURL, String urlParameters)
	{
	    URL url;
	    HttpURLConnection connection = null;  
	    try 
	    {
	      url = new URL(targetURL);
	      connection = (HttpURLConnection)url.openConnection();
	      connection.setRequestMethod("POST");
	      connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				
	      connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
	      connection.setRequestProperty("Content-Language", "en-US");  
	
	      connection.setUseCaches (false);
	      connection.setDoInput(true);
	      connection.setDoOutput(true);

	      DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());
	      wr.writeBytes (urlParameters);
	      wr.flush ();
	      wr.close ();

	      //Get Response	
	      InputStream is = connection.getInputStream();
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	      String line;
	      StringBuffer response = new StringBuffer(); 
	      	while((line = rd.readLine()) != null) 
	      	{
	    	  	response.append(line);
	        	response.append('\r');
	      	}
	      	rd.close();
	      	return response.toString();

	      } 
	    catch (Exception e) 
	    {
	   		e.printStackTrace();
	    	return null;
	    } 
	    	
	    finally 
	    {

	    	if(connection != null) 
	    		{
	    		connection.disconnect(); 
	    		}
	    }
	 }
	
	/**
	 * @param args
	 * @throws TembooException 
	 * @throws IOException 
	 */
		public static void main(String[] args) throws TembooException, IOException
		{
			Scanner in = new Scanner(System.in);
			ArrayList<String> list[];
			
			String ConsumerKey="bGEhU85osh3jMgMM9rXIP59Ak";
			String ConsumerSecret="VnFvH8S4wazpXX6wAw4GJQPDHlusJm6KjJuqNlDtr2EmVJIQMt";
	
			//Temboo Session
			TembooSession session = new TembooSession("rrk43", "myFirstApp", "da3db495cbf748f7b776b5f7ae337ead");
			
			//Initialize OAuth
			InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
			InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
			initializeOAuthInputs.set_ConsumerSecret(ConsumerSecret);
			initializeOAuthInputs.set_ConsumerKey(ConsumerKey);
			InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	
			String AuthorizationURL=initializeOAuthResults.get_AuthorizationURL();
			String callbackID=initializeOAuthResults.get_CallbackID();
			String OAuthTokenSecret=initializeOAuthResults.get_OAuthTokenSecret();
			

			System.out.println("Go to the following URL and obtain the code that you find there.\n" + AuthorizationURL);
			String code = in.nextLine();
			
			//FinalizeOAuth
			FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);
			FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();
			finalizeOAuthInputs.set_CallbackID(callbackID);
			finalizeOAuthInputs.set_OAuthTokenSecret(OAuthTokenSecret);
			finalizeOAuthInputs.set_ConsumerSecret(ConsumerKey);
			finalizeOAuthInputs.set_ConsumerKey(ConsumerSecret);
			FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
			String AccessToken=finalizeOAuthResults.get_AccessToken();
			String AccessTokenSecret=finalizeOAuthResults.get_AccessTokenSecret();
			
			//UserTimeline
			UserTimeline userTimelineChoreo = new UserTimeline(session);
			UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();
			userTimelineInputs.set_ScreenName("markiplier");
			userTimelineInputs.set_Count("30");
			userTimelineInputs.set_AccessToken(AccessToken);
			userTimelineInputs.set_AccessTokenSecret(AccessTokenSecret);
			userTimelineInputs.set_ConsumerSecret(ConsumerSecret);
			userTimelineInputs.set_ConsumerKey(ConsumerKey);
			UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
			String Response=userTimelineResults.get_Response();
			
			//Json used to store tweets.
			JsonParser jp = new JsonParser();
	    	JsonElement root = jp.parse(Response);
	    	JsonArray tweets = root.getAsJsonArray(); 
	    	int numPoly = 0;

	    	//For loop used to gather and parse tweets and print them out.
	    	for(int i = 0; i < tweets.size(); i++)
	    	{
	    		String output = tweets.get(i).getAsJsonObject().get("text").getAsString();
	    		System.out.println("Tweet " + i + ": " + output + "\n");
	    		
	    		String[] outputSplit = output.split("\\s");
	    		
	    		for(int m =0; m < outputSplit.length ;m++)
	    		{

	    			if (outputSplit[m].contains("//"))
	    			{
	    				System.out.println("special stuff: " + outputSplit[m]);
	    				//Do nothing if there's a special char inside
	    			}
	    			else if (outputSplit[m].contains("!"))
	    			{
	    				System.out.println("!: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("."))
	    			{
	    				System.out.println(".: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("?"))
	    			{
	    				System.out.println("?: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains(","))
	    			{
	    				System.out.println(",: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains(":"))
	    			{
	    				System.out.println(":: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("@"))
	    			{
	    				System.out.println("@: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("#"))
	    			{
	    				System.out.println("#: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("\""))
	    			{
	    				System.out.println("\": " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("&"))
	    			{
	    				System.out.println("&: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("-"))
	    			{
	    				System.out.println("&: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("'"))
	    			{
	    				System.out.println("&: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].contains("�"))
	    			{
	    				System.out.println("weird qoute: " + outputSplit[m]);
	    				//Do nothing if it is a null string
	    			}
	    			else if (outputSplit[m].length() <= 4)
	    			{
	    				System.out.println("length<3: " + outputSplit[m]);
	    				//Do nothing if has three letters or less
	    			}
	    			
	    			else
	    			{
	    				String sURL = "http://api.wordnik.com:80/v4/word.json/" + outputSplit[m] + "/hyphenation?useCanonical=false&limit=50&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
	    				URL url = new URL(sURL);
	    				HttpURLConnection request = (HttpURLConnection) url.openConnection();
	    				request.connect();
	                
	    				JsonParser jp2 = new JsonParser();
	    				JsonElement root2 = jp2.parse(new InputStreamReader((InputStream) request.getContent()));
	    				JsonArray rootobj2 = root2.getAsJsonArray();
	    				System.out.println("m" + m);
	    				if(rootobj2.size()>=2)
	    				{
	    					numPoly=numPoly+1;
	    					System.out.println("numPoly: " + numPoly);
	    				}
	    			}
	    		}
	    		
	    	}
	    	double SMOG = 1.0430 * Math.sqrt(numPoly * (30/30))+3.1291;
	    	System.out.println("The SMOG for rhvriv was: " + SMOG);

		}



}

Ryan Koshy
